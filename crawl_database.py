''' Create a database and Insert elements '''
import sqlite3
from sqlite3 import Error

class Database:

	def create_connection(self, name_of_database='crawl_database.db'):
		try:
			conn = sqlite3.connect(name_of_database)
			return conn
		except Error:
			print(Error)
			return None


	def close_connection(self, conn):
		conn.close()


	def commit_changes(self, conn):
		conn.commit()


	def create_html_table(self, conn, table_name = 'Html_Table'):
		cursor = conn.cursor()
		cursor.execute('Create table ' + table_name + ' (Url text primary key, Html_Page text, Status integer)')


	def create_source_and_destiantion_table(self, conn, table_name = 'SOURCE_and_DESTINATION'):
		cursor = conn.cursor()
		cursor.execute('Create table ' + table_name + ' (SOURCE text, DESTINATION text)')


	def insert_into_html_table(self, conn, insert_tuple, table_name = 'Html_Table'):
		cursor = conn.cursor()
		cursor.execute('insert into ' + table_name + ' values(?,?,?)', insert_tuple)
		self.commit_changes(conn)


	def insert_into_source_and_destination_table(self, conn, insert_tuple, table_name = 'SOURCE_and_DESTINATION'):
		cursor = conn.cursor()
		cursor.execute('insert into '+ table_name +' values(?,?)', insert_tuple)
		self.commit_changes(conn)

	def check_url_in_db(self, conn, url):
		cursor = conn.cursor()
		cursor.execute('SELECT Url from Html_Table where Url =:url',{"url" : url})
		return cursor.fetchone()

	def retrieve_edge_data(self):
		conn = self.create_connection('crawl_database.db')
		cursor = conn.cursor()
		cursor.execute('SELECT * FROM SOURCE_and_DESTINATION')
		edge_data = cursor.fetchall()
		self.close_connection(conn)
		return edge_data
