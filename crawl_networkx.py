import networkx as nx
import crawl_database as cdb


def intialize_Graph():
	graph = nx.Graph()
	return graph

def add_edges(graph, edge_list):
	graph.add_edges_from(edge_list)


def create_edges():
	graph = intialize_Graph()
	edge_data = cdb.Database().retrieve_edge_data()
	add_edges(graph, edge_data)
	return graph


def determine_shortest_path(graph, node1, node2):
	# This shortest_path function returns a list of path
	shortest_path_list = nx.shortest_path(graph, node1, node2)
	return shortest_path_list
