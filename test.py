import unittest
import web_crawler
import crawl_database
import crawl_networkx


class crawler(unittest.TestCase):

    def test_check_200_status(self):
        test = web_crawler.Crawler()
        is_status_200 = test.check_for_200status('http://0.0.0.0:8000/')
        self.assertTrue(is_status_200)

    def test_html_link_extract(self):
        test = web_crawler.Crawler()
        html_response = '<a href="sample.com"></a>'
        act_links = test.html_link_extract(html_response)
        exp_links = ["sample.com"]
        self.assertEqual(act_links, exp_links)
        self.assertEqual(test.html_link_extract('<p src="http:sample.com">foo</p>'), [])

    def test_crawl_only_urls(self):
        test = web_crawler.Crawler()
        is_crawlable = test.crawl_only_urls("http://0.0.0.0:8000", "http://0.0.0.0:8000/foo")
        self.assertTrue(is_crawlable)
        is_crawlable = test.crawl_only_urls("http://0.0.0.0:8000", "/foo")
        self.assertFalse(is_crawlable)

    def test_fix_url(self):
        test = web_crawler.Crawler()
        act_fixed_url = test.fix_url("/foo", "https://example.com")
        exp_fixed_url = "https://example.com/foo"
        self.assertEqual(act_fixed_url, exp_fixed_url)
        act_fixed_url = test.fix_url("http://0.0.0.0:8000/foo", "http://0.0.0.0:8000")
        exp_fixed_url = None
        self.assertEqual(act_fixed_url, exp_fixed_url)

    def test_page_scrapper(self):
        test = web_crawler.Crawler()
        links, html_page, status_code = test.page_scraper("http://0.0.0.0:8000/book11.html")
        self.assertEqual(links, [])
        self.assertEqual(status_code, 200)


class database(unittest.TestCase):

    def test_crawl_and_save(self):
        test_crawler = web_crawler.Crawler()
        test_crawler.crawl_and_save("http://0.0.0.0:8000/", 10)
        test_db = crawl_database.Database()
        self.test_conn = test_db.create_connection()
        self.test_cursor = self.test_conn.cursor()
        actual_source_destination_data = test_db.retrieve_edge_data()
        expected_source_destination_data = [('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book1.html'),
                                            ('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book2.html'),
                                            ('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book3.html'),
                                            ('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book4.html'),
                                            ('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book5.html'),
                                            ('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book6.html'),
                                            ('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book7.html'),
                                            ('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book8.html'),
                                            ('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book9.html'),
                                            ('http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book10.html')
                                            ]
        self.assertEqual(len(actual_source_destination_data), 100)
        for i in range(10):
            self.assertEqual(actual_source_destination_data[i], expected_source_destination_data[i])

    def tearDown(self):
        self.test_cursor.execute("DROP TABLE SOURCE_and_DESTINATION")
        self.test_cursor.execute("DROP TABLE Html_Table")
        self.test_conn.close()


class networkx(unittest.TestCase):

    def setUp(self):
        test_crawler = web_crawler.Crawler()
        test_crawler.crawl_and_save("http://0.0.0.0:8000/", 100)

    def test_shortest_path(self):
        test_graph = crawl_networkx.create_edges()
        node1 = "http://0.0.0.0:8000/book2.html"
        node2 = "http://0.0.0.0:8000/book22.html"
        actual_path = crawl_networkx.determine_shortest_path(test_graph, node1, node2)
        expected_path = ["http://0.0.0.0:8000/book2.html","http://0.0.0.0:8000/book22.html"]
        self.assertEqual(actual_path, expected_path)

    def tearDown(self):
        test_conn = crawl_database.Database().create_connection()
        test_cursor = test_conn.cursor()
        test_cursor.execute("DROP TABLE SOURCE_and_DESTINATION")
        test_cursor.execute("DROP TABLE Html_Table")
        test_conn.close()

if __name__ == '__main__':
    unittest.main()
