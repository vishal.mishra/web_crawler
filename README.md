
------Project WEB CRAWLER--------

Project WEB CRAWLER, is made to crawl all the web pages and extract all the links
and after extraction it crawls those links too! and stores them on a database
from any desired main host website. Also it uses the database to Organise
in Graph Structure Using python library NETWORKX.



=== Virtual Environment ===

 To create a virtualenv, use on command line

# virtualenv virtualenv_name 

 To activate the virtualenv

# cd virtualevn_name
# source ./bin/activate

 To deactivate the virtualenv

# deactivate





=== Installing requirement.txt in virtualenv ===

# pip install -r requirement.txt


=== How to Run the program ===

 Execute the main.py file in a python interpreter
 and the Enter the website you wanna Crawl on, and to what extend in max pages
 prompt as a Real Number.

 After It crawls your Desired Website, You can find the shortest path between two links
 you have crawled.


=== To Test the program ===
 
 Just run the test.py and It will run the code with 97% coverage
 
