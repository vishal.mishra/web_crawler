import web_crawler as webc
import crawl_networkx as cnx


def main(main_host, max_pages):
	crawl = webc.Crawler()
	crawl.crawl_and_save(main_host, max_pages)
	graph = cnx.create_edges()
	return graph

if __name__ == '__main__':

	main_host = input('Enter the Website You Want to Crawl : ')
	max_pages = int(input('MAX PAGES TO CRAWL: '))
	graph = main(main_host, max_pages)
	node1 = input('To Evalute the Shortest Path, Enter the Source URL: ')
	node2 = input('Enter the Destined URL: ')
	shortest_path_list = cnx.determine_shortest_path(graph, node1, node2)
	print(shortest_path_list)
