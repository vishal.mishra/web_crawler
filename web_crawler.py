# ---------building a web crawler------------


import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import crawl_database as cdb


class Crawler:

	# unit function
	def get_html(self, url):
		get_response = requests.get(url)
		status_code = get_response.status_code
		get_response_text = get_response.text
		return get_response_text, status_code

	# a unit function, that takes html response text and extract links from it
	def html_link_extract(self, get_response_text):
		soup = BeautifulSoup(get_response_text, 'html.parser')
		link_tag = soup.find_all('a')
		list_of_link  = []
		for i in link_tag:
			list_of_link.append(i.get('href'))
		return list_of_link

	# `get html` is used
	def check_for_200status(self, url):
		get_response_text, status_code = self.get_html(url)
		if status_code == 200:
			return True

	# a unit function, compares the hostnames both url 
	def url_netloc(self, url, main_host):
		parsed_url = urlparse(url)
		main_host = urlparse(main_host)
		if parsed_url.netloc == main_host.netloc:
			return True
	
	# ==unit funtion=== checks the scheme of url 
	def scheme_check(self, url):
		parsed_url = urlparse(url)
		if parsed_url.scheme == 'https' or parsed_url.scheme == 'http':
			return True

	# `scheme check` and `url netloc` is used here
	def crawl_only_urls(self, url, main_host):
			return self.scheme_check(url) and self.url_netloc(url, main_host)

	# ==unit function==
	def is_host_name_present(self, url):
		parsed_url = urlparse(url)
		if parsed_url.netloc:
			return True

	# `is host name present` is used 
	def fix_url(self, url, main_host):
		if self.is_host_name_present(url):
			return None
		else:
			if url is None:
				return
			else:
				return main_host + url

	def page_scraper(self, url):
		if self.check_for_200status(url):
			get_response_text, status_code = self.get_html(url)
			html_page = get_response_text
			list_of_urls = self.html_link_extract(get_response_text)
			return list_of_urls, html_page, status_code
		else:
			return 'Not Available', 'Not Available', 'Not Available'

	def crawl_and_save(self, main_host, max_pages):
		urls_collected = [main_host]
		pages_crawled = 0
		conn = cdb.Database().create_connection('crawl_database.db')
		cdb.Database().create_html_table(conn)
		cdb.Database().create_source_and_destiantion_table(conn)
		for link in urls_collected:
			if not cdb.Database().check_url_in_db(conn, link):
				pages_crawled += 1
				print('crawling page - {} : {}'.format(pages_crawled, link))
				list_of_urls, html_page, status_code = self.page_scraper(link)
				insert_data = (link, html_page, status_code)
				cdb.Database().insert_into_html_table(conn, insert_data)
				for j in list_of_urls:
					fixed_url = self.fix_url(j, main_host)
					if self.crawl_only_urls(fixed_url, main_host):
						cdb.Database().insert_into_source_and_destination_table(conn, (link, fixed_url))
						urls_collected.append(fixed_url)
				if pages_crawled == max_pages:
					cdb.Database().close_connection(conn)
					return None
